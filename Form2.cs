﻿/*
 * Created by SharpDevelop.
 * User: notmmao
 * Date: 2015/6/1
 * Time: 10:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace D3AutoClick
{
	/// <summary>
	/// Description of Form2.
	/// </summary>
	public partial class Form2 : Form
	{
		public static Form2 _instance = null;
		public static Form2 Instance {
			get {
				if(_instance == null) {
					_instance = new Form2();
				}
				return _instance;
			}
		}
		
		private Form2()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			init();
		}
		
		Config _config;
		
		public Config KeyConfig {
			get { return _config; }
			set { 
				if(_config != null) {
					save(_config);
				}
				_config = value;
				bind(value);
			}
		}
		private void init() {
			this.Icon = D3AutoClick.Properties.Resources.Logo;
			try {
				KeyConfig = Config.DeSerialize("default");
			}
			catch(Exception e) {
				KeyConfig = Config.Default("default");
			}
			Win32Api.RegisterHotKey(Handle, 202, 0, Keys.Oemtilde);
			
			bind(KeyConfig);
			bindList();
			
			this.up5.ShowLeft();
			this.up6.ShowRight();
			this.up7.ShowShift();
		}
		
		private bool started = false;
		public void OnHotkey() {   
			started = !started;	
			this.up1.Started = started;
			this.up2.Started = started;
			this.up3.Started = started;
			this.up4.Started = started;
			this.up5.Started = started;
			this.up6.Started = started;
			this.up7.Started = started;
			
			this.Enabled = !started;
			if(started) {
				save(KeyConfig);
			}
			
        }
        
        protected override void WndProc(ref Message m)
        {
            const int WM_HOTKEY = 0x0312;
            if(m.Msg == Win32Api.WM_GCSM_SHOWME) {
            		Show();
            		this.WindowState = FormWindowState.Normal;
            		this.Activate();
            		return;
            }
            switch (m.Msg)
            {
                case WM_HOTKEY:
            		OnHotkey();
                    break;
            }
            base.WndProc(ref m);
        }
        void bind(Config config) {
        	this.up1.Key = config.Key1.ToString();
			this.up1.Interval = config.Key1Value;
			this.up2.Key = config.Key2.ToString();
			this.up2.Interval = config.Key2Value;
			this.up3.Key = config.Key3.ToString();
			this.up3.Interval = config.Key3Value;
			this.up4.Key = config.Key4.ToString();
			this.up4.Interval = config.Key4Value;
			this.up5.Interval = config.KeyLeftValue;
			this.up6.Interval = config.KeyRightValue;
			this.up7.Interval = config.KeyShiftValue;
			
			this.up1.Enable = config.Key1Enable;
			this.up2.Enable = config.Key2Enable;
			this.up3.Enable = config.Key3Enable;
			this.up4.Enable = config.Key4Enable;
			this.up5.Enable = config.KeyLeftEnable;
			this.up6.Enable = config.KeyRightEnable;
			this.up7.Enable = config.KeyshiftEnable;
			
			this.up1.Pressed = config.Key1Pressed;
			this.up2.Pressed = config.Key2Pressed;
			this.up3.Pressed = config.Key3Pressed;
			this.up4.Pressed = config.Key4Pressed;
			this.up5.Pressed = config.KeyLeftPressed;
			this.up6.Pressed = config.KeyRightPressed;
			this.up7.Pressed = config.KeyShiftPressed;
        }
        void save(Config config) {
        	config.Key1 = this.up1.Key;
			config.Key2 = this.up2.Key;
			config.Key3 = this.up3.Key;
			config.Key4 = this.up4.Key;
			
			config.Key1Value = this.up1.Interval;
			config.Key2Value = this.up2.Interval;
			config.Key3Value = this.up3.Interval;
			config.Key4Value = this.up4.Interval;
			config.KeyLeftValue = this.up5.Interval;
			config.KeyRightValue = this.up6.Interval;
			config.KeyShiftValue = this.up7.Interval;
			
			config.Key1Enable = this.up1.Enable;
			config.Key2Enable = this.up2.Enable;
			config.Key3Enable = this.up3.Enable;
			config.Key4Enable = this.up4.Enable;
			config.KeyLeftEnable = this.up5.Enable;
			config.KeyRightEnable = this.up6.Enable;
			config.KeyshiftEnable = this.up7.Enable;
			
			config.Key1Pressed = this.up1.Pressed;
			config.Key2Pressed = this.up2.Pressed;
			config.Key3Pressed = this.up3.Pressed;
			config.Key4Pressed = this.up4.Pressed;
			config.KeyLeftPressed = this.up5.Pressed;
			config.KeyRightPressed = this.up6.Pressed;
			config.KeyShiftPressed = this.up7.Pressed;
			
			Config.Serialize(config);
        }
		
		void LbConfigSelectedIndexChanged(object sender, EventArgs e)
		{
			if(this.lbConfig.SelectedIndex >= 0) {
				Config c;
				string name = this.lbConfig.SelectedItem.ToString();
			
				try {
					c = Config.DeSerialize(name);
				}
				catch(Exception ex) {
					c = Config.Default(name);
				}
				c.Name = name;
				KeyConfig = c;
				this.tbConfig.Text = name;
			}
		}
		
		void BtnAddClick(object sender, EventArgs e)
		{
			string name = this.tbConfig.Text;
			int index = this.lbConfig.Items.IndexOf(name);
			if(index < 0) {
				this.lbConfig.Items.Add(name);
			}
		}
		
		void bindList() {
			System.IO.DirectoryInfo di = new DirectoryInfo(Environment.CurrentDirectory);
			FileInfo[] fi = di.GetFiles("*.lj");
			foreach(FileInfo f in fi) {
				Console.WriteLine(f.FullName);
				string str = f.Name;
				str = str.Remove(str.Length - f.Extension.Length);
				this.lbConfig.Items.Add(str);
			}
		}
		
		void Form2FormClosed(object sender, FormClosedEventArgs e)
		{
			save(KeyConfig);
		}
		
		void Form2FormClosing(object sender, FormClosingEventArgs e)
		{
			if(e.CloseReason == CloseReason.UserClosing) {
				e.Cancel = true;
				this.WindowState = FormWindowState.Minimized;
				this.Hide();
			}
		}
	}
}
