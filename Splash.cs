﻿/*
 * Created by SharpDevelop.
 * User: notmmao
 * Date: 2015/6/2
 * Time: 11:39
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Windows.Forms;

namespace D3AutoClick
{
	/// <summary>
	/// Description of Splash.
	/// </summary>
	public partial class Splash : Form
	{

		public string VersionText = "D3AutoClick " + Application.ProductVersion;
		
		static Splash splashScreen;
		Bitmap bitmap;
		
		private static string _message = 
			"郑重声明:程序仅仅模拟按键输出,没有任何读取与修改Diablo3进程数据动作\r\n" +
												"战网Tag:老寒腿#5105,妈妈叫我小涮皮儿#5259";
		
		public static string Message {
			get { return _message; }
		}
		public static Splash SplashScreen {
			get {
				return splashScreen;
			}
			set {
				splashScreen = value;
			}
		}
		
		public Splash()
		{
			FormBorderStyle = FormBorderStyle.None;
			StartPosition   = FormStartPosition.CenterScreen;
			ShowInTaskbar   = false;
			
			string versionText = VersionText;
			
			// Stream must be kept open for the lifetime of the bitmap
            bitmap = D3AutoClick.Properties.Resources.SplashScreen;
            //bitmap = PrintControlTest.Properties.Resources.sss;
            //bitmap = new Bitmap(typeof(SplashScreenForm).Assembly.GetManifestResourceStream("Resources.SplashScreen.jpg"));
			this.ClientSize = bitmap.Size;
			using (Font font = new Font("Sans Serif", 40)) {
				using (Graphics g = Graphics.FromImage(bitmap)) {
					g.DrawString(versionText, font, Brushes.White, 20, 202);
				}
			}
			using (Font font = new Font("Sans Serif", 14)) {
				using (Graphics g = Graphics.FromImage(bitmap)) {
					g.DrawString(Message, font, Brushes.White, 25, 262);
				}
			}
			BackgroundImage = bitmap;
		}
		
		public static void ShowSplashScreen()
		{
			splashScreen = new Splash();
			splashScreen.Show();
		}
		
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (bitmap != null) {
					bitmap.Dispose();
					bitmap = null;
				}
			}
			base.Dispose(disposing);
		}

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SplashScreenForm
            // 
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Name = "SplashScreenForm";
            this.Load += new System.EventHandler(this.SplashScreenForm_Load);
            this.ResumeLayout(false);

        }

        private void SplashScreenForm_Load(object sender, EventArgs e)
        {
            this.ClientSize = bitmap.Size;

            Size size = new Size(bitmap.Width/50, bitmap.Height/50);
            for (double d = 0.50; d < 1; d += 0.08)
            {
                System.Threading.Thread.Sleep(1);
                Application.DoEvents();
                //SizeF size = new SizeF(bitmap.Width, bitmap.Height);
                this.ClientSize -= size;
                this.Refresh();
            }
        }
	
	}
}
