/*
 * Created by SharpDevelop.
 * User: notmmao
 * Date: 2015/6/1
 * Time: 9:53
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace D3AutoClick
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	[Serializable]
	public class Config
	{
		private int _keyLeft;
		public int KeyLeft
		{
			get { return _keyLeft; }
			set { _keyLeft = value; }
		}
		
		private int _keyRight;
		public int KeyRight
		{
			get { return _keyRight; }
			set { _keyRight = value; }
		}
		
		private int _keyShift;
		public int KeyShift
		{
			get { return _keyShift; }
			set { _keyShift = value; }
		}
		
		
		private int _keyLeftValue;
		
		public int KeyLeftValue {
			get { return _keyLeftValue; }
			set { _keyLeftValue = value; }
		}
		private bool _keyLeftEnable;
		
		public bool KeyLeftEnable {
			get { return _keyLeftEnable; }
			set { _keyLeftEnable = value; }
		}
		
		private int _keyRightValue;
		
		public int KeyRightValue {
			get { return _keyRightValue; }
			set { _keyRightValue = value; }
		}
		private bool _keyRightEnable;
		
		public bool KeyRightEnable {
			get { return _keyRightEnable; }
			set { _keyRightEnable = value; }
		}
		
		private int _keyShiftValue;
		
		public int KeyShiftValue {
			get { return _keyShiftValue; }
			set { _keyShiftValue = value; }
		}
		private bool _keyshiftEnable;
		
		public bool KeyshiftEnable {
			get { return _keyshiftEnable; }
			set { _keyshiftEnable = value; }
		}
		
		private int _key1Value;
		
		public int Key1Value {
			get { return _key1Value; }
			set { _key1Value = value; }
		}
		private string _key1;
		public string Key1
		{
			get { return _key1; }
			set { _key1 = value; }
		}
		private int _key2Value;
		
		public int Key2Value {
			get { return _key2Value; }
			set { _key2Value = value; }
		}
		
		private string _key2;
		public string Key2
		{
			get { return _key2; }
			set { _key2 = value; }
		}
		private int _key3Value;
		
		public int Key3Value {
			get { return _key3Value; }
			set { _key3Value = value; }
		}
		private string _key3;
		public string Key3
		{
			get { return _key3; }
			set { _key3 = value; }
		}
		
		private string _key4;
		private int _key4Value;
		
		public int Key4Value {
			get { return _key4Value; }
			set { _key4Value = value; }
		}
		public string Key4
		{
			get { return _key4; }
			set { _key4 = value; }
		}
		
		private bool _key1Enable = false;
		
		public bool Key1Enable {
			get { return _key1Enable; }
			set { _key1Enable = value; }
		}
		private bool _key2Enable = false;
		
		public bool Key2Enable {
			get { return _key2Enable; }
			set { _key2Enable = value; }
		}
		private bool _key3Enable = false;
		
		public bool Key3Enable {
			get { return _key3Enable; }
			set { _key3Enable = value; }
		}
		private bool _key4Enable = false;
		
		public bool Key4Enable {
			get { return _key4Enable; }
			set { _key4Enable = value; }
		}
		
		private bool _key1Pressed;
		
		public bool Key1Pressed {
			get { return _key1Pressed; }
			set { _key1Pressed = value; }
		}
		private bool _key2Pressed;
		
		public bool Key2Pressed {
			get { return _key2Pressed; }
			set { _key2Pressed = value; }
		}
		private bool _key3Pressed;
		
		public bool Key3Pressed {
			get { return _key3Pressed; }
			set { _key3Pressed = value; }
		}
		private bool _key4Pressed;
		
		public bool Key4Pressed {
			get { return _key4Pressed; }
			set { _key4Pressed = value; }
		}
		private bool _keyRightPressed;
		
		public bool KeyRightPressed {
			get { return _keyRightPressed; }
			set { _keyRightPressed = value; }
		}
		private bool _keyLeftPressed;
		
		public bool KeyLeftPressed {
			get { return _keyLeftPressed; }
			set { _keyLeftPressed = value; }
		}
		private bool _keyShiftPressed;
		
		public bool KeyShiftPressed {
			get { return _keyShiftPressed; }
			set { _keyShiftPressed = value; }
		}
		
		
		private string _name;
		public string Name 
		{
			get {return _name;}
			set {_name = value;}
		}
		public Config()
		{
		}
		
		#region public static method
		public static void Serialize(Config c)
        {
            using (FileStream fs = new FileStream(c.Name+".lj", FileMode.Create))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(fs, c);
            }
        }

        public static Config DeSerialize(String name)
        {
            Config c;
            using (FileStream fs = new FileStream(name+".lj", FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                c = (Config)formatter.Deserialize(fs);
            }
            return c;
        }
        
        public static Config Default(String name) {
        	    Config c = new Config();
				c.Key1 = "49";
				c.Key1Value = 100;
				c.Key2 = "50";
				c.Key2Value = 100;
				c.Key3 = "51";
				c.Key3Value = 100;
				c.Key4 = "52";
				c.Key4Value = 100;
				c.Name = name;
				Config.Serialize(c);
				return c;
        }
		#endregion
	}
}
