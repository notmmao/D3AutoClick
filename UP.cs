/*
 * Created by SharpDevelop.
 * User: notmmao
 * Date: 2015/6/1
 * Time: 11:32
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace D3AutoClick
{
	/// <summary>
	/// Description of UserControl1.
	/// </summary>
	public partial class UP : UserControl
	{
		public UP()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
	
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			this.lbLeft.Hide();
			this.lbRight.Hide();
			this.lbShift.Hide();
			
			thread = new MyThread();
			thread.Started = this.Enable;
			thread.SleepTime = this.Interval;
		}
		
		private string _key;
		public string Key {
			get {return _key;}
			set {
				_key = value;
				if(value != null && value.Length > 0) {
					thread.Key = byte.Parse(value);
				}
				this.tbKey1.Text = value;
			}
		}
		MyThread thread;
		
		private bool _started = false;
		
		public bool Started {
			get { return _started; }
			set { 
				if(Enable) {
					if(Pressed) {
						if(value) {
							this.DoPress();
						}
						else {
							this.DoRelease();
						}
					} 
					else {
						_started = value;
						this.thread.Started = value;
						if(value) {
							thread.Start();
						}
						else {
							thread.Stop();
						}
					}
				}
			}
		}
		public bool Pressed {
			get {return this.cbKey.Checked;}
			set {
				this.cbKey.Checked = value;
				this.tbDKey1.Enabled = !value;
			}
		}
		
		public bool Enable {
			get {return this.cbEnable.Checked;}
			set {this.cbEnable.Checked = value;}
		}
		private int _interval = 100;
		public int Interval {
			get { return _interval;}
			set { 
				_interval = value;
				thread.SleepTime = value;
				this.tbDKey1.Text = value.ToString();
			}
		}
		
		
		void TbKey1KeyDown(object sender, KeyEventArgs e)
		{
            this.tbKey1.Text = e.KeyValue.ToString();    
            Key = this.tbKey1.Text;
		}
		void TbDKey1KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(e.KeyChar!=8&&!Char.IsDigit(e.KeyChar))
	    	{
	      		e.Handled = true;
	    	}
		}
		
		void CbEnableCheckedChanged(object sender, EventArgs e)
		{
			//this.thread.Started = this.cbEnable.Checked;
			Console.WriteLine("enable:" + this.cbEnable.Checked);
		}
		
		void TbDKey1TextChanged(object sender, EventArgs e)
		{
			if(!String.IsNullOrEmpty(this.tbDKey1.Text)) {
				Interval = int.Parse(this.tbDKey1.Text);
				Console.WriteLine("interval:" + this.tbDKey1.Text);
			}
		}
		
		public void DoPress() {
			if(this.Pressed) {
				if(thread.RightMouse) {
					Win32Api.mouseRightDown();
				} 
				else if(thread.LeftMouse) {
					Win32Api.mouseLeftDown();
				}
				else {
					if(!string.IsNullOrEmpty(Key)) {
						Win32Api.keyPress(byte.Parse(Key));
					}
				}
			}
		}
		public void DoRelease(){
			if(thread.RightMouse) {
				Win32Api.mouseRightUp();
			} 
			else if(thread.LeftMouse) {
				Win32Api.mouseLeftUp();
			}
			else {
				if(!string.IsNullOrEmpty(Key)) {
						Win32Api.keyRelease(byte.Parse(Key));
				}
			}
		}
		
		void CbKeyCheckedChanged(object sender, EventArgs e)
		{
			this.Pressed = this.cbKey.Checked;
		}
		
		public void ShowLeft() {
			this.lbLeft.Show();
			this.lbRight.Hide();
			this.tbKey1.Hide();
			this.lbShift.Hide();
			thread.LeftMouse = true;
		}
		public void ShowRight() {
			this.lbRight.Show();
			this.lbLeft.Hide();
			this.tbKey1.Hide();
			this.lbShift.Hide();
			thread.RightMouse = true;
		}
		public void ShowShift() {
			this.lbRight.Hide();
			this.lbLeft.Hide();
			this.tbKey1.Hide();
			this.lbShift.Show();
			Key = "16";
		}
	}
}
