﻿/*
 * Created by SharpDevelop.
 * User: notmmao
 * Date: 2015/6/1
 * Time: 10:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace D3AutoClick
{
	partial class Form2
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.up1 = new D3AutoClick.UP();
			this.up2 = new D3AutoClick.UP();
			this.up3 = new D3AutoClick.UP();
			this.up4 = new D3AutoClick.UP();
			this.up5 = new D3AutoClick.UP();
			this.up6 = new D3AutoClick.UP();
			this.up7 = new D3AutoClick.UP();
			this.lbConfig = new System.Windows.Forms.ListBox();
			this.tbConfig = new System.Windows.Forms.TextBox();
			this.btnAdd = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// up1
			// 
			this.up1.Enable = false;
			this.up1.Interval = 100;
			this.up1.Key = null;
			this.up1.Location = new System.Drawing.Point(31, 18);
			this.up1.Name = "up1";
			this.up1.Pressed = false;
			this.up1.Size = new System.Drawing.Size(335, 30);
			this.up1.Started = false;
			this.up1.TabIndex = 0;
			// 
			// up2
			// 
			this.up2.Enable = false;
			this.up2.Interval = 100;
			this.up2.Key = null;
			this.up2.Location = new System.Drawing.Point(31, 46);
			this.up2.Name = "up2";
			this.up2.Pressed = false;
			this.up2.Size = new System.Drawing.Size(335, 30);
			this.up2.Started = false;
			this.up2.TabIndex = 1;
			// 
			// up3
			// 
			this.up3.Enable = false;
			this.up3.Interval = 100;
			this.up3.Key = null;
			this.up3.Location = new System.Drawing.Point(31, 77);
			this.up3.Name = "up3";
			this.up3.Pressed = false;
			this.up3.Size = new System.Drawing.Size(335, 30);
			this.up3.Started = false;
			this.up3.TabIndex = 2;
			// 
			// up4
			// 
			this.up4.Enable = false;
			this.up4.Interval = 100;
			this.up4.Key = null;
			this.up4.Location = new System.Drawing.Point(30, 108);
			this.up4.Name = "up4";
			this.up4.Pressed = false;
			this.up4.Size = new System.Drawing.Size(335, 30);
			this.up4.Started = false;
			this.up4.TabIndex = 3;
			// 
			// up5
			// 
			this.up5.Enable = false;
			this.up5.Interval = 100;
			this.up5.Key = null;
			this.up5.Location = new System.Drawing.Point(31, 139);
			this.up5.Name = "up5";
			this.up5.Pressed = false;
			this.up5.Size = new System.Drawing.Size(335, 26);
			this.up5.Started = false;
			this.up5.TabIndex = 6;
			// 
			// up6
			// 
			this.up6.Enable = false;
			this.up6.Interval = 100;
			this.up6.Key = null;
			this.up6.Location = new System.Drawing.Point(30, 166);
			this.up6.Name = "up6";
			this.up6.Pressed = false;
			this.up6.Size = new System.Drawing.Size(335, 30);
			this.up6.Started = false;
			this.up6.TabIndex = 8;
			// 
			// up7
			// 
			this.up7.Enable = false;
			this.up7.Interval = 100;
			this.up7.Key = null;
			this.up7.Location = new System.Drawing.Point(31, 194);
			this.up7.Name = "up7";
			this.up7.Pressed = false;
			this.up7.Size = new System.Drawing.Size(335, 30);
			this.up7.Started = false;
			this.up7.TabIndex = 9;
			// 
			// lbConfig
			// 
			this.lbConfig.FormattingEnabled = true;
			this.lbConfig.ItemHeight = 12;
			this.lbConfig.Location = new System.Drawing.Point(404, 23);
			this.lbConfig.Name = "lbConfig";
			this.lbConfig.Size = new System.Drawing.Size(173, 136);
			this.lbConfig.TabIndex = 10;
			this.lbConfig.SelectedIndexChanged += new System.EventHandler(this.LbConfigSelectedIndexChanged);
			// 
			// tbConfig
			// 
			this.tbConfig.Location = new System.Drawing.Point(404, 187);
			this.tbConfig.Name = "tbConfig";
			this.tbConfig.Size = new System.Drawing.Size(109, 21);
			this.tbConfig.TabIndex = 11;
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(535, 186);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(75, 23);
			this.btnAdd.TabIndex = 12;
			this.btnAdd.Text = "添加";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.BtnAddClick);
			// 
			// Form2
			// 
			this.ClientSize = new System.Drawing.Size(666, 248);
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.tbConfig);
			this.Controls.Add(this.lbConfig);
			this.Controls.Add(this.up7);
			this.Controls.Add(this.up6);
			this.Controls.Add(this.up5);
			this.Controls.Add(this.up4);
			this.Controls.Add(this.up3);
			this.Controls.Add(this.up2);
			this.Controls.Add(this.up1);
			this.Name = "Form2";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2FormClosed);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.TextBox tbConfig;
		private System.Windows.Forms.ListBox lbConfig;
		private D3AutoClick.UP up7;
		private D3AutoClick.UP up6;
		private D3AutoClick.UP up5;
		private D3AutoClick.UP up4;
		private D3AutoClick.UP up3;
		private D3AutoClick.UP up2;
		private D3AutoClick.UP up1;
	}
}
