﻿/*
 * Created by SharpDevelop.
 * User: notmmao
 * Date: 2015/6/1
 * Time: 11:32
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace D3AutoClick
{
	partial class UP
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbKey1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tbDKey1 = new System.Windows.Forms.TextBox();
			this.cbKey = new System.Windows.Forms.CheckBox();
			this.cbEnable = new System.Windows.Forms.CheckBox();
			this.lbLeft = new System.Windows.Forms.Label();
			this.lbRight = new System.Windows.Forms.Label();
			this.lbShift = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// tbKey1
			// 
			this.tbKey1.Location = new System.Drawing.Point(55, 3);
			this.tbKey1.Name = "tbKey1";
			this.tbKey1.ReadOnly = true;
			this.tbKey1.Size = new System.Drawing.Size(71, 21);
			this.tbKey1.TabIndex = 7;
			this.tbKey1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbKey1KeyDown);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(191, 6);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(35, 23);
			this.label1.TabIndex = 6;
			this.label1.Text = "间隔:";
			// 
			// tbDKey1
			// 
			this.tbDKey1.Location = new System.Drawing.Point(228, 3);
			this.tbDKey1.Name = "tbDKey1";
			this.tbDKey1.Size = new System.Drawing.Size(100, 21);
			this.tbDKey1.TabIndex = 5;
			this.tbDKey1.TextChanged += new System.EventHandler(this.TbDKey1TextChanged);
			this.tbDKey1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbDKey1KeyPress);
			// 
			// cbKey
			// 
			this.cbKey.Location = new System.Drawing.Point(137, 1);
			this.cbKey.Name = "cbKey";
			this.cbKey.Size = new System.Drawing.Size(50, 24);
			this.cbKey.TabIndex = 4;
			this.cbKey.Text = "按住";
			this.cbKey.UseVisualStyleBackColor = true;
			this.cbKey.CheckedChanged += new System.EventHandler(this.CbKeyCheckedChanged);
			// 
			// cbEnable
			// 
			this.cbEnable.Location = new System.Drawing.Point(3, 1);
			this.cbEnable.Name = "cbEnable";
			this.cbEnable.Size = new System.Drawing.Size(50, 24);
			this.cbEnable.TabIndex = 8;
			this.cbEnable.Text = "启用";
			this.cbEnable.UseVisualStyleBackColor = true;
			this.cbEnable.CheckedChanged += new System.EventHandler(this.CbEnableCheckedChanged);
			// 
			// lbLeft
			// 
			this.lbLeft.Location = new System.Drawing.Point(55, 6);
			this.lbLeft.Name = "lbLeft";
			this.lbLeft.Size = new System.Drawing.Size(71, 23);
			this.lbLeft.TabIndex = 9;
			this.lbLeft.Text = "鼠标左键:";
			// 
			// lbRight
			// 
			this.lbRight.Location = new System.Drawing.Point(58, 5);
			this.lbRight.Name = "lbRight";
			this.lbRight.Size = new System.Drawing.Size(71, 23);
			this.lbRight.TabIndex = 10;
			this.lbRight.Text = "鼠标右键:";
			// 
			// lbShift
			// 
			this.lbShift.Location = new System.Drawing.Point(59, 4);
			this.lbShift.Name = "lbShift";
			this.lbShift.Size = new System.Drawing.Size(71, 23);
			this.lbShift.TabIndex = 11;
			this.lbShift.Text = "Shift键:";
			// 
			// UP
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.lbShift);
			this.Controls.Add(this.lbRight);
			this.Controls.Add(this.lbLeft);
			this.Controls.Add(this.cbEnable);
			this.Controls.Add(this.tbKey1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.tbDKey1);
			this.Controls.Add(this.cbKey);
			this.Name = "UP";
			this.Size = new System.Drawing.Size(335, 30);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Label lbShift;
		private System.Windows.Forms.Label lbRight;
		private System.Windows.Forms.Label lbLeft;
		private System.Windows.Forms.CheckBox cbEnable;
		private System.Windows.Forms.CheckBox cbKey;
		private System.Windows.Forms.TextBox tbDKey1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbKey1;
	
	}
}
