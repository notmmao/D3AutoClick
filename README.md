
D3AutoClick -- Diablo3自动按键助手
===================================

通过设置1-4技能按键和鼠标左右键以及shift键状态--按住或者定时自动按键--来减少玩家手动输入.


程序使用了Win32API中热键相关接口,可能会被360之类安全软件误报为记录键盘等恶意程序.
但程序仅仅注册了一个热键Oemtilde(~)用于快捷开启或关闭"自动按键"状态,所以请放心使用.
专业人士可以直接查看相关代码RegisterHotKey API的使用,绝不会做任何记录玩家信息的动作.

开发环境
--------------

- Windows 7 (也可以在WinXP下进行,需要安装.net Framework 2.0)
- .net Framework 2.0
- SharpDevelop 2.2 (也可以使用Visua Studio 2005以及以上版本)

ScreenShot 截图
--------------

无图言屌

![alt 启动界面](Screenshot/splash.png "启动界面")
![alt 主界面](Screenshot/mainForm.png "主界面")

License 协议
-------

软件使用MIT协议

该软件及其相关文档对所有人免费，可以任意处置，包括使用，复制，修改，合并，发表，分发，再授权，或者销售。
唯一的限制是，软件中必须包含上述版 权和许可提示。

http://opensource.org/licenses/mit-license.php


