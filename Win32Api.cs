/*
 * Created by SharpDevelop.
 * User: notmmao
 * Date: 2015/6/1
 * Time: 10:06
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Threading;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace D3AutoClick
{
	/// <summary>
	/// Description of Win32Api.
	/// </summary>
	public class Win32Api
	{
		private Win32Api()
		{
		}
		
		 const int WM_KEYDOWN = 0x0100;
        const int WM_KEYUP = 0x0101;
        const int WM_CHAR = 0x0102;
        
        const int WM_LBUTTONDOWN = 0x201;
        const int WM_LBUTTONUP = 0x202;
        const int WM_LBUTTONDBLCLK = 0x0206;
        
        const int WM_RBUTTONDOWN = 0x204;
        const int WM_RBUTTONUP = 0x205;
        const int WM_RBUTTONDBLCLK = 0x0206;
		
		const int MOUSEEVENTF_MOVE = 0x0001;     // ???? 
		const int MOUSEEVENTF_LEFTDOWN = 0x0002; //???????? 
		const int MOUSEEVENTF_LEFTUP = 0x0004; //???????? 
		const int MOUSEEVENTF_RIGHTDOWN = 0x0008;// ???????? 
		const int MOUSEEVENTF_RIGHTUP = 0x0010; //???????? 
		const int MOUSEEVENTF_MIDDLEDOWN = 0x0020;// ???????? 
		const int MOUSEEVENTF_MIDDLEUP = 0x0040; //???????? 
		const int MOUSEEVENTF_ABSOLUTE = 0x8000; //?????????? 
		
        [DllImport("User32")]
        private extern static bool GetCursorPos(out Point p);
        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll", EntryPoint = "FindWindowEx")]
        private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
        [DllImport("user32.dll", EntryPoint = "PostMessage")]
        private static extern int PostMessage(IntPtr hwnd, uint wMsg, int wParam, int lParam);
        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage(IntPtr hwnd, uint wMsg, int wParam, int lParam);
        [DllImport("user32.dll", EntryPoint = "WindowFromPoint")]
        private static extern IntPtr WindowFromPoint(int px, int py);
        [DllImport("kernel32")]
        private static extern bool WriteprivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFileName);
        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int modifiers, Keys vk);
        [DllImport("user32.dll")]
        static extern bool UnregisterHotKey(IntPtr hWnd, int id);
        [DllImport("user32.dll")] 
		private static extern int mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo); 
		[DllImport("User32.dll")]
		private static extern void keybd_event(Byte bVk, Byte bScan, Int32 dwFlags, Int32 dwExtraInfo);
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int RegisterWindowMessage(string message);

		//winAPI
 		private static readonly IntPtr HWND_BROADCAST = new IntPtr(0xffff);
        internal static readonly int WM_GCSM_SHOWME = RegisterWindowMessage("WM_GCSM_SHOWME");
        
        public static void ShowRemote()
        {
            // send a broadcast message to show the settings form in another instance
            Win32Api.PostMessage(HWND_BROADCAST, (uint)WM_GCSM_SHOWME, 0, 0);
        }	
        
		public static void mouseLeftDown() {
			mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
		}
		public static void mouseLeftUp() {
			mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
		}
		public static void mouseRightDown() {
			mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
		}
		public static void mouseRightUp() {
			mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
		}
        public static void mouseLeftClick() {
        	mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
        	mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
        }
        
        public static void mouseRightClick() {
        	mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
        	mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
        }
        
        public static void keyClick(byte key) {
        	//keybd_event(key,0,0,0);
        	//Thread.Sleep(5);
   			//keybd_event(key,0,2,0);
   			
   			IntPtr hwnd = WindowFromPoint(Cursor.Position.X, Cursor.Position.Y);
   			PostMessage(hwnd, WM_KEYDOWN, key, 0);
            Thread.Sleep(5);
            PostMessage(hwnd, WM_KEYUP, key, 0);
        }

		public static void keyPress(byte key) {
        	IntPtr hwnd = WindowFromPoint(Cursor.Position.X, Cursor.Position.Y);
   			PostMessage(hwnd, WM_KEYDOWN, key, 0);
        }
		public static void keyRelease(byte key){
   			IntPtr hwnd = WindowFromPoint(Cursor.Position.X, Cursor.Position.Y);
   			PostMessage(hwnd, WM_KEYUP, key, 0);
        }
		
	
	}
}
