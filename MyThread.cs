/*
 * Created by SharpDevelop.
 * User: notmmao
 * Date: 2015/6/1
 * Time: 12:29
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Threading;
using System.Windows.Forms;

namespace D3AutoClick
{
	/// <summary>
	/// Description of MyThread.
	/// </summary>
	public class MyThread
	{
		private Thread thread;
		public MyThread()
		{
			thread = new Thread(run);
		}
		
		public bool Done {
			get {return _done;}
			set {_done = value;}
		}
		
		bool _done = false;
		bool _started = false;
		byte _key = 0;
		
		public byte Key {
			get { return _key; }
			set { _key = value; }
		}

		public bool Started {
			get { return _started; }
			set { _started = value; }
		}
		int _sleepTime = 100;
		
		public int SleepTime {
			get { return _sleepTime; }
			set { 
				_sleepTime = value;
				if(_sleepTime < 100) {
					_sleepTime = 100;
				}
			}
		}
		
		private bool _leftMouse = false;
		
		public bool LeftMouse {
			get { return _leftMouse; }
			set { _leftMouse = value; }
		}
		private bool _rightMouse = false;
		
		public bool RightMouse {
			get { return _rightMouse; }
			set { _rightMouse = value; }
		}
		
		void doSomething() {
			if(this.RightMouse) {
				Win32Api.mouseRightClick();
				
				Console.WriteLine("mouseRightClick:" + DateTime.Now.ToLongTimeString());
			}
			else if(this.LeftMouse){
				Win32Api.mouseLeftClick();
				Console.WriteLine("mouseLeftClick:" + DateTime.Now.ToLongTimeString());
			}
			else {
				Win32Api.keyClick(_key);
				Console.WriteLine("KeyCliek:" + _key +"," + DateTime.Now.ToLongTimeString());
			}
		}
		void run() {
			while(!_done) {
				
				if(_started) {
					doSomething();
					Thread.Sleep(_sleepTime);
				}
				else {
					Thread.Sleep(100);
				}
				
				Application.DoEvents();
			}
		}
		public void Stop() {
			if(thread.IsAlive) {
				thread.Abort();
			}
		}
		public void Start() {
			if(thread.IsAlive) {
				thread.Abort();
			}
			thread = new Thread(run);
			thread.Start();
		}
	}
}
